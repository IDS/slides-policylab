

![picture 4](slides.assets/cover.png){.r-stretch} 

David Benqué - EU Policy Lab  
25-04-2024 


---


![](slides.assets/Malife_logos.svg){.r-stretch}  

::: notes  

CryptPad and IDS

today is more centered around IDS

:::  

---

![](slides.assets/DB-cv-meme.png){.r-stretch}

::: notes
3 step CV
- BA: using technology in design practice 
  - dutch, python, type design
- MA: taking technology as the subject of practice
  - using speculation for this
- PhD: taking speculation itself as the subject of research and practice 

:::

---

## The Futures Cone

![image-20240423202346445](slides.assets/image-20240423202346445.png){.r-stretch}

:::ref

Candy, S. (2010) *The futures of everyday life: Politics and the design of experiential scenarios*.

:::

:::notes

I want to start with the example of the futures cone to position myself today with relation to nearly 15 years ago at the Royal College of Art with speculative design

- who knows the futures cone?
- foundational diagram of speculative design
- this is the version through which it made its way into the Design Interactions curriculum around the 2010's
- from Candy to Dunne and Raby
- at the time we used this to justify speculative projects, coming up with scenarios
- today I wouldn't be interested in that so much as in doing an archaeology of this diagram

:::

---

![img](slides.assets/original_071f7fb6ff7d6569176a4230a31b316d.png){.r-stretch}

:::ref

Taylor, C.W. (1990) *Creating strategic visions*. Carlisle Barracks, Pennsylvania: STRATEGIC STUDIES INSTITUTE U.S ARMY WAR COLLEGE.

:::

:::notes

from US Army documents

:::

---

![image-20240423203449953](slides.assets/image-20240423203449953.png){.r-stretch}

:::ref

Hancock, T. and Bezold, C. (1994)  Possible futures, preferable futures. , *The Healthcare Forum journal*, 37(2), pp. 23 29.

:::

:::notes

healthcare futures

::::

---

![img](slides.assets/original_adaf73cf54c1772c13489840569c7b50.png){.r-stretch}

:::ref

Voros, J. (2003)  A generic foresight process framework , *Foresight*, 5(3), pp. 10 21. Available at: https://doi.org/10.1108/14636680310698379.

:::

---

![FuturesCone-CDB](slides.assets/futurescone-cdb.png){.r-stretch}

:::ref

Voros 2017

:::

---

![image-20240423213621827](slides.assets/image-20240423213621827.png){.r-stretch}

:::notes

instead of using the cone directly as a method 

I'd be interested in tracking this cone as a media construct

across the different narratives where it gets mobilised from Army to design via healthcare and future studies

it landing as a speculative design thinking device

and the thing that in my view speculative design failed to do is to question the shape of this cone 

- the types of speculation that it implies or promotes
- the politics of "possibility space" and who gets to set the boundaries
- universal cone map whereas Candy himself in his dissertation was calling for a multiplicity of maps
- and of special importance to me = that this possibility space is quite literally being drawn up by algorithmic systems nowadays: machine learning and so called AI quite literally create space from data 

:::



---

## Institute of Diagram Studies

![](slides.assets/IDS_logo.svg){.r-stretch}  

[https://diagram.institute](https://diagram.institute)  

::: notes  

this is where I position the institute 


PhD stuff --> diagrams and algorithmic prediction  

:::  

---

![](slides.assets/diagram-mediation.png){.r-stretch}  

::: ref  
@benque2020a
:::  

::: notes  
the summary of the Diagram Studies approach 
look at everything through the lens of diagrams
consider relations, mediations, systems, operations

a constraint + a very elastic idea

:::  

---

### Key Words

---



#### Operative Images

> These are images that do not represent an object, but rather are part of an operation. 

::: ref  
@farocki2004  
:::  

::: notes  

- this is not visual studies, even though the work is visual

a lot of this work is visual, but it doesn't stop there
diagrams extend far beyond just visualisations and that's what gives them purchase and relevance, 

esp. with algorithmic or computational systems

a research practice concerned with operational images.  

instead, diagrams can be described as operative images.   

not about describing or representing but thinking about the larger sets of relations.   

:::  

---

#### Media Archaeology

- Probe the **depths** (historical, technical, infrastructural, ...) of media and technology

:::ref

@bardini2016

:::

:::notes

not going into too much detail 

- archaeology of knoweldge
- practice based research
- engaging critically with old and new systems, documents
- can be theoretical or practical

:::



---

#### Vector space



![](slides.assets/scikit-out.png){.r-stretch}  

::: ref  
Classifier comparison - scikit-learn  
[https://scikit-learn.org/stable/auto_examples/classification/plot_classifier_comparison.html](https://scikit-learn.org/stable/auto_examples/classification/plot_classifier_comparison.html)  

@pedregosa2011  
:::  

::: notes

a key notion to understand "AI" and the high dimensional spaces in which so much of our data is processed and becomes operative

:::

---

![](slides.assets/data-pipeline.png){.r-stretch}  

:::ref  
@oneil2013  
:::  

:::notes  

zoom out to the formation producing this vectorized operations

::::  



---

#### Vectoral Politics

![](slides.assets/vectoralist-class.png){.r-stretch}

:::ref
@anthony2017  

@wark2004
:::

:::notes

and the politics of this vectorized world = vectoralist analysis by Mackenzie Wark

:::


---

#### Oscillations / Moves

>It is not a question, however, of two fundamentally different types of diagram; rather, this <mark>oscillation between systematising and openness</mark> is inherent in the diagram.

:::ref
@leeb2017
:::

:::notes

diagrammatic vocabulary also to guide practice

oscilations = both in each diagram all the time

retrospective: a way to organise and systematize 
projective: vectors pointing in unknown directions

:::

----

> Was the diagram also a form of violence? 

:::ref
@sillman2020
:::

:::notes
so diagrams are not just a fun and aesthetic speculative device

they can also be used to engage with quite violent systems

- borders
- credit scoring
- policing
- insurance
- etc

:::



---



![image-20240423215854463](slides.assets/image-20240423215854463.png){.r-stretch}

::: ref  
[https://www.are.na/institute-of-diagram-studies/index](https://www.are.na/institute-of-diagram-studies/index)  
:::  

:::notes

these are some key words

for more interests and things and collections check the are.na 

:::

---

### Projects

:::notes  

diagrams to read the algorithmic apparatus of control   

also speculative devices that can generate new possibilities  

:::  

---

#### Sleuthing

  

::: notes  
mapping / plotting  

investigation with one's own means  

::::    


---

![](slides.assets/1564423218081.png){.r-stretch}  

:::ref  
Diagrams of the Future [DOTF.XYZ](http://dotf.xyz)  
:::  

:::notes  
diagrams of the future

mapping of the history of algorithmic prediction through its diagrams.   
:::  

----

![](slides.assets/1564424028869.png){.r-stretch}  

:::ref  
Diagrams of the Future [DOTF.XYZ](http://dotf.xyz)  
:::  

:::notes  
this tool looks like any other blog on the face of it but it is powered by a graph   

invites putting things in relation and constructing genealogies   

:::  

---

![](slides.assets/dotf-timeline.png){.r-stretch}  

:::ref  
Diagrams of the Future [DOTF.XYZ](http://dotf.xyz)  
:::  

:::notes  
the result is a bit messy but it counteracts any idea that history, and in this case especially the history of techonolgy is a linear process. instead it is conducive to making more connections, readings and re-readings  

:::  

---

> Identifying topoi, analyzing their trajectories and transformations, and explaining the cultural logics that condition their “wanderings” across time and space is one possible goal for media archaeology.  

::: ref  
@huhtamo2011a  
:::  

::: notes  
my research looks a lot to media archaeology for methods and framing

one good concept that moves diagrammatically is this idea of following lineages 


topoi: tropes, threads   

diagrammatic form for the **genealogy** of technology  
:::  

----

![](slides.assets/tumblr_o16n2kBlpX1ta3qyvo1_1280.jpg){.r-stretch}  

:::notes  
in this mode, sleuthing is not a definitive act, it does not result in a clear picture of the whodunnit.   

instead it is an ongoing process of piecing together, and of not completely adding up   

home brewed software to conduct the investigation: reflects/informs the knowledge created  

graph database   


continually making and re-making the software  

it acknowledges its own subjectivity and perhaps its own delusions, but it also begins to enable some kind of orientation and reclaiming.   

:::  

---

#### Probing

:::notes  
mapping might use some computational techniques but it stays at a distance.   

the next posture I want to propose is one that moves closer to algorithmic systems  
:::  

----

>  how does one study someone who doesn’t want to be studied and (unlike Spotify’s users) has the power to enforce that wish?  

:::ref  
@james2019  
:::  

:::notes  

and again there are some relational politics here that could be expressed as diagrams.   

the question of access   
more and more limited  
reverse engineering  

:::  

---

![](slides.assets/1571864130260.png){.r-stretch}  



:::ref  
Architectures of Choice: Vol.1 YouTube   
[https://davidbenque.com/projects/architectures-of-choice/](https://davidbenque.com/projects/architectures-of-choice/)  
:::  

:::notes  
probes to generate traces   

:::  

---

![](slides.assets/2019-01-21-12-11-06.png){.r-stretch}  

:::ref  
@marenko2019

:::  

:::notes  

:::  

---

> 'hunters, [...] by interpreting a series of traces reconstruct the appearance of an animal they have never seen'  

:::ref  
@ginzburg1980  
:::  

:::notes  

Traces: types of data collected by YouTube and Netflix to maximise time spent on the site  


the key here is not to focus only on access  
but to embrace a conjectural model  
:::  



-----


<video stretch controls autoplay loop>   
<source src="slides.assets/screencast_trace.mp4" type="video/mp4">  
</video>  

:::ref  
Architectures of Choice: Vol.1 YouTube   
[https://davidbenque.com/projects/architectures-of-choice/](https://davidbenque.com/projects/architectures-of-choice/)  
:::  

:::notes  

invite speculation as to what the relation between the videos might be  

:::  

---

![](slides.assets/adr-screenshot.png){.r-stretch}  

:::ref  
Atlas des Recommandations [https://adr.diagram.institute](https://adr.diagram.institute/)  
:::  

:::notes  
workshop continuing this work   
spreading it to other platforms   
:::  

---

![](slides.assets/Injection-Lorette.jpg){.r-stretch}  

::: ref  
*Fluo-Green project*, Université de Namur   
[https://sites.google.com/site/fluogproject/](https://sites.google.com/site/fluogproject/)
:::  

:::notes  
probing   
releasing tracing agents to produce traces  

from there imagining algorithms that we will never see   
:::  

---

#### Divining

:::notes  
finally the last activity or practice that I'd like to propose is that of the diviner.   
This completes the trajectory that started at a distance, moved up to probe outputs, to finally the operations themselves and the production of predictions  

:::  

---

> Chicane comes to signify both an element within symbolic and divinatory performance, and the game of interpretation for both insider and outsider concerning the mysterious phenomena involved.  

:::ref  
@cornelius2016  
:::  

:::notes  
the chicane refers to a trick or sharp turn  
it is often used to describe the sharp turns on a race track   

used by cornelius to theorise the performance of divination and the kinds of trick that the diviner and client partake in to produce the prediction   
:::  

----

![](slides.assets/diagram-chicane.png){.r-stretch}  

:::ref  

:::  

:::notes  
the chicane may or may not involve an object, such as a crystal ball, or a computer   

In any case thinking about the chicane focuses the process of mediation at play in prediction   

:::  

---

![](slides.assets/data-pipeline.png){.r-stretch}  

:::ref  
@oneil2013  
:::  

:::notes  
the data pipeline I showed you earlier is in fact a chicane  
the question is is it a sincere one?   

- amazingly, data and algorithms are often presented as unmediated ways to get to the truth, so the fact that the chicane is not acknowledged is not a good sign 
- also to put it in Rameys words the bluff here often involves the diviner pretending to know something that the client doesn't, and doesn't always have their best interest in mind 

:::  

---

![](slides.assets/almanac-computer.png){.r-stretch}  

:::ref  
almanac.computer [https://almanac.computer](https://almanac.computer)  
:::  

:::notes  
I've explored algorihtmic prediction as divination in a project called almanac computer.   

This project revisits almanac publications, an early prototype of data analytics   
:::  

---

![](slides.assets/cosmic-commodity.png){.r-stretch}  

:::ref  
Cosmic Commodity Charts - [https://almanac.computer](https://almanac.computer)  
:::  

:::notes  
predicting prices on commodity futures markets from the positions of the planets in the solar system.   
:::  

---

![picture 6](slides.assets/CCC-notebook.png){.r-stretch}



:::ref  
Cosmic Commodity Charts - [https://almanac.computer](https://almanac.computer)  
:::  

:::notes  
this involves quite a bit of chicanery to use the tools of algorithmic prediction at the service of completely different rationalities.   

In doing so my goal was to question which modes of speculation are deemed legitimate and which aren't   
:::  


---

#### Operational Loops of COVID-19

![](slides.assets/original_4eb8d58a5a84e9a1e60add3ba933cf6c.png){.r-stretch}  

::: ref  
diagrams for: @sampson2021  

:::  

::: notes  

since Phd  

extending remit of diagrams   

:::   

---

![picture 1](slides.assets/covid-waves.png){.r-stretch}

:::ref
[Operational Loops of COVID19](https://git.diagram.institute/davidbenque/Operational_Loops_of_COVID19/src/branch/black/main.pdf)
::::

---

![picture 2](slides.assets/covid-models.png){.r-stretch}

:::ref
[Operational Loops of COVID19](https://git.diagram.institute/davidbenque/Operational_Loops_of_COVID19/src/branch/black/main.pdf)
:::

---

#### Webcam Manager

![](slides.assets/image-20210523230741481.png){.r-stretch}  

:::  ref  

[*Webcam Manager*](https://wm.diagram.institute)  

:::  

::: notes  

more applied and more directly intervening in power relations  

Telecommuters  RYBN and others

Webcam Manager: simple looping of a *gaze* that we have all experienced the zoom call  

::::  

---

![picture 3](slides.assets/wm-diagram.png){.r-stretch}  

:::  ref  
[*Webcam Manager*](https://wm.diagram.institute)  
:::  

---

![picture 4](slides.assets/wm-ui.png){.r-stretch}  

:::  ref  
[*Webcam Manager*](https://wm.diagram.institute)  
:::  

---

![picture 1](slides.assets/wm-toc.png){.r-stretch}


:::notes
extensive documentation

- operations manual 
- articles about camera looping in movies
- and sabotage
:::

---

#### Sigil Séance 

![](slides.assets/full_sigil.svg)

:::ref
In collaboration with [Lucile Olympe Haute](http://lucilehaute.fr)  
[Source code](https://git.diagram.institute/davidbenque/Sigil_Generator)
:::

---

Bind Billionaires who fly to the upper sky from coming back to Earth

![alt](slides.assets/full_sigil_grid.svg)
![picture 2](slides.assets/animation_dark.gif)

✨ 🔮 🚀 🍆 🌌 ⍖ 🌏 🌎 🌍 🔮 ✨


---

![picture 3](slides.assets/seance.png){.r-stretch}

:::notes
plans for online seance tool
:::


---

## FIN

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fork-awesome@1.1.7/css/fork-awesome.min.css" integrity="sha256-gsmEoJAws/Kd3CjuOQzLie5Q3yshhvmo7YNtBG7aaEY=" crossorigin="anonymous">


![](slides.assets/IDS_logo.svg){.r-stretch}

David Benqué  
[https://diagram.institute](https://diagram.institute)  
​<i class="fa fa-instagram" aria-hidden="true"></i> [\@diagram.studies](https://www.instagram.com/diagram.studies/)  
<i class="fa fa-mastodon" aria-hidden="true"></i> [\@air_pump@post.lurk.org](https://post.lurk.org/@air_pump)  



